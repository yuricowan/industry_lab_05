package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by ycow194 on 24/03/2017.
 */
public abstract class Animal {

    int time;

    public abstract void feed();

    public abstract int costToFeed();

    public abstract String getName();

    public abstract int getValue();

    public void tick() {
        time += 1;

    }

}
