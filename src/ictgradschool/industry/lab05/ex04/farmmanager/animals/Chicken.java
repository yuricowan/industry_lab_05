package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by ycow194 on 24/03/2017.
 */
public class Chicken extends Animal implements ProductionAnimals{
    private static final String name = "Chicken";

    private int value;

    public Chicken() {
        value = 500;
    }

    public void feed() {
        if (value < 1200) {
            value += 100;
        }
    }

    public int costToFeed() {
        return 60;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        if (super.time >= 3) {
            return true;
        }
        return false;
    }

    @Override
    public int harvest() {

        if (harvestable()) {
            time = time - 3;
            return +3;
        }
        return 0;
    }
}