package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by ycow194 on 24/03/2017.
 */
public class Dragon extends Animal implements ProductionAnimals {
    private static final String name = "Dragon";

    private int value;

    public Dragon() {
        value = 3000;
    }

    public void feed() {
        if (value < 7000) {
            value += 2000;
        }
    }

    public int costToFeed() {
        return 900;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        if (super.time >= 1) {
            return true;
        }
        return false;
    }

    @Override
    public int harvest() {

        if (harvestable()) {
            time = time - 1;
            return +250;
        }
        return 0;
    }
}
