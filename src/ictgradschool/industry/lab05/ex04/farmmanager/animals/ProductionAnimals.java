package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by ycow194 on 24/03/2017.
 */
public interface ProductionAnimals {
    /*
     * Is an animal able to be harvested?
     */


    public boolean harvestable();



    /**
     * Harvests a crop from the animal.
     *
     * @return The money made from harvesting the animal. If the animal isn't harvestable return zero.
     */
    public int harvest();
}
