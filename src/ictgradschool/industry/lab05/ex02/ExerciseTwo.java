package ictgradschool.industry.lab05.ex02;

import com.sun.org.apache.bcel.internal.generic.IAND;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {


    public void start() {


        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        IAnimal[] animals = new IAnimal[3];
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.

        for (int i = 0; i < list.length; i++) {


            System.out.println(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " says " + list[i].sayHello());
            System.out.print(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " is a ");

            if (list[i].isMammal()) {
                System.out.println("mammal");
            } else if (!list[i].isMammal()) {

                System.out.println("non-mammal");
            }


            System.out.println("Did i forget to tell you that I have " + list[i].legCount() + " legs.");
            System.out.println();


            // TODO If the animal also implements IFamous, print out that corresponding info too.
            if (list[i] instanceof IFamous) {

                IFamous a = (IFamous) list[i]; // its like converting a double into an integer
                System.out.println("This is a famous name of my animal type: " + a.famous());
            }
        }

    }


    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
